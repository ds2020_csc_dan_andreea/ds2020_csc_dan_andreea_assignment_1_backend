package ro.andreea.ds2020.entities;
import net.minidev.json.annotate.JsonIgnore;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name="medication")
public class Medication implements Serializable{
    private static final long serialVersionUID = 4L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(
            name="medication_side_effect",
            joinColumns = {@JoinColumn(name="medication_id")},
            inverseJoinColumns = {@JoinColumn(name="side_effects_id")}
    )
    @Column(nullable = true)
    private List<SideEffects> side_effects=new ArrayList<SideEffects>();

    @Column(name="name", nullable = false)
    private String name;

    @Column(name="dosage", nullable = false)
    private double dosage;

    public Medication(){

    }

    public Medication(String name, double dosage, List<SideEffects> side_effects){
        this.name=name;
        this.dosage=dosage;
        this.side_effects= side_effects;
    }

    public Medication(UUID id, String name, double dosage, List<SideEffects> side_effects){
        this.id=id;
        this.name=name;
        this.dosage=dosage;
        this.side_effects= side_effects;
    }

    public Medication(UUID id, String name, double dosage) {
        this.id=id;
        this.name=name;
        this.dosage=dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDosage() {
        return dosage;
    }

    public void setDosage(double dosage) {
        this.dosage = dosage;
    }

    public List<SideEffects> getSideEffects() {
        return side_effects;
    }

    public void setSideEffects(List<SideEffects> side_effects) {
        this.side_effects = side_effects;
    }
}
