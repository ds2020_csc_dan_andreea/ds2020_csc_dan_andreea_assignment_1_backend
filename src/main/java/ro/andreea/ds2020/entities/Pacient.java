package ro.andreea.ds2020.entities;

import org.apache.tomcat.jni.User;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

@Entity
@Table(name="pacient")
public class Pacient implements Serializable {
    private static final long serialVersionUID = 3L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name="name", nullable = false)
    private String name;

    @Column(name="birthdate", nullable = false)
    private Date birthdate;

    @Column(name="gender", nullable = false)
    private String gender;

    @Column(name="address", nullable = false)
    private String address;

    @Column(name="medrecord", nullable = false)
    private String medrecord;

    @Column(name="cusername", nullable = false)
    private String cusername;

    @Column(name="username", nullable = false, unique = true)
    private String username;

    @Column(name="role")
    private String role;

    public Pacient(){

    }

    public Pacient(String name, Date birthdate, String gender, String address, String medrecord, String cusername, String username){
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;
        this.medrecord=medrecord;
        this.cusername= cusername;
        this.username= username;
    }

    public Pacient(UUID id, String name, Date birthdate, String gender, String address, String medrecord, String cusername, String username){
        this.id=id;
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;
        this.medrecord=medrecord;
        this.cusername=cusername;
        this.username=username;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedrecord() {
        return medrecord;
    }

    public void setMedrecord(String medrecord) {
        this.medrecord = medrecord;
    }

    public String getCusername() {
        return cusername;
    }

    public void setCaregiver_username(String cusername) {
        this.cusername = cusername;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}


