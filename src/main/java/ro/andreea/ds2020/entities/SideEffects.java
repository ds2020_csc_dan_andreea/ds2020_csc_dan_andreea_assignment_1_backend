package ro.andreea.ds2020.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import net.minidev.json.annotate.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name="sideEffects")
public class SideEffects implements Serializable{
    private static final long serialVersionUID = 5L;

    @Id
    ///@GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name="name", nullable = false)
    private String name;

    public SideEffects(){

    }

    public SideEffects(String name){
        this.name=name;
    }

    public SideEffects(UUID id, String name){
        this.id=id;
        this.name=name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
