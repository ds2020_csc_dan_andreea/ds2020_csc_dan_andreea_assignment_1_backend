package ro.andreea.ds2020.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name="treatmentPlan")
public class TreatmentPlan implements Serializable {
    private static final long serialVersionUID = 6L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @ManyToMany(fetch= FetchType.EAGER)
    @JoinTable(
            name="treatment_plan_medication",
            joinColumns = {@JoinColumn(name="treatment_plan_id")},
            inverseJoinColumns = {@JoinColumn(name="medication_id")}
    )
    private Set<Medication> medications=new HashSet<>();

    @Column(name="firsthour", nullable = false)
    private int firsthour;

    @Column(name="secondhour", nullable = false)
    private int secondhour;

    @Column(name="period",nullable = false)
    private int period;

    @Column(name="pusername", nullable = false)
    private String pusername;

    public TreatmentPlan(){

    }

    public TreatmentPlan(Set<Medication> medications, int firsthour, int secondhour, int period, String pusername){
        this.firsthour=firsthour;
        this.secondhour=secondhour;
        this.period=period;
        this.medications=medications;
        this.pusername = pusername;
    }

    public TreatmentPlan(UUID id, Set<Medication> medications, int firsthour, int secondhour, int period, String pusername){
        this.id=id;
        this.medications=medications;
        this.firsthour=firsthour;
        this.secondhour=secondhour;
        this.period=period;
        this.pusername = pusername;
    }

    public TreatmentPlan(int firsthour, int secondhour, int period) {
        this.firsthour=firsthour;
        this.secondhour=secondhour;
        this.period=period;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPusername() {
        return pusername;
    }

    public void setPusername(String pacient_username) {
        this.pusername = pacient_username;
    }

    @JsonBackReference
    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    public int getFirstHour() { return firsthour; }

    public void setFirstHour(int firsthour) { this.firsthour = firsthour; }

    public int getSecondHour() { return secondhour; }

    public void setSecondHour(int secondHour) { this.secondhour = secondhour; }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
}
