package ro.andreea.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.andreea.ds2020.entities.Medication;

import java.util.Optional;
import java.util.UUID;

public interface MedicationRepository extends JpaRepository<Medication, UUID> {

    Optional<Medication> findAllByName(String name);

    void deleteByName(String name);

    Medication findByName(String name);

    boolean findByName(boolean b);

}
