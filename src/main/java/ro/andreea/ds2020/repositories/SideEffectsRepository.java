package ro.andreea.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.andreea.ds2020.entities.SideEffects;

import java.util.List;
import java.util.UUID;

public interface SideEffectsRepository extends JpaRepository<SideEffects, UUID> {
    SideEffects findByName(String sideEffectsName);

}

