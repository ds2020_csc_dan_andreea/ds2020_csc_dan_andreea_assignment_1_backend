package ro.andreea.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.andreea.ds2020.entities.Login;

import java.util.UUID;

public interface LoginRepository extends JpaRepository<Login, UUID> {

    Login findByUsername(String username);
}
