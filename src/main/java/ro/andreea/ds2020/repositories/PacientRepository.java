package ro.andreea.ds2020.repositories;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import ro.andreea.ds2020.dtos.PacientDTO;
import ro.andreea.ds2020.entities.Pacient;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PacientRepository extends JpaRepository<Pacient, UUID> {

    List<Pacient> findAll();
    List<Pacient> findAll(Sort sort);
    Optional<Pacient> findAllByName(String name);
    Optional<Pacient> findByUsername(String username);
    void deleteById(PacientDTO pacientDTO);

    List<Pacient> findAllByCusername(String caregiver_username);

    void deleteByUsername(String username);
}
