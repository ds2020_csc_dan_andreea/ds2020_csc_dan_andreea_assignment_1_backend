package ro.andreea.ds2020.repositories;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import ro.andreea.ds2020.entities.Caregiver;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CaregiverRepository extends JpaRepository<Caregiver, UUID> {

    List<Caregiver> findAll();
    List<Caregiver> findAll(Sort sort);
    Optional<Caregiver> findAllByName(String name);
    Optional<Caregiver> findAllByUsername(String username);

    void deleteByUsername(String username);

    Caregiver findByUsername(String username);

}
