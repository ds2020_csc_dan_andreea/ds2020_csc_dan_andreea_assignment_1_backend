package ro.andreea.ds2020.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import ro.andreea.ds2020.entities.TreatmentPlan;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TreatmentPlanRepository extends JpaRepository<TreatmentPlan, UUID> {
    List<TreatmentPlan> findByPusername(String name);
}
