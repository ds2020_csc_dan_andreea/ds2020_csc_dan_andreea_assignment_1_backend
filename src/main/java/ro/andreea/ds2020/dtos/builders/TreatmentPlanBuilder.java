package ro.andreea.ds2020.dtos.builders;
import ro.andreea.ds2020.dtos.TreatmentPlanDTO;
import ro.andreea.ds2020.entities.Medication;
import ro.andreea.ds2020.entities.TreatmentPlan;

import java.util.HashSet;
import java.util.Set;

public class TreatmentPlanBuilder {

    private TreatmentPlanBuilder() {
    }

    public static TreatmentPlanDTO toTreatmentPlanDTO(TreatmentPlan treatmentPlan) {
        TreatmentPlanDTO treatmentPlanDTO = new TreatmentPlanDTO();
        Set<String> medList = new HashSet<String>();
        treatmentPlanDTO.setId(treatmentPlan.getId());
        treatmentPlanDTO.setFirsthour(treatmentPlan.getFirstHour());
        treatmentPlanDTO.setSecondhour(treatmentPlan.getSecondHour());
        treatmentPlanDTO.setPeriod(treatmentPlan.getPeriod());
        treatmentPlanDTO.setPacient_username(treatmentPlan.getPusername());
        for(Medication med: treatmentPlan.getMedications()){
            medList.add(med.getName());
        }
        treatmentPlanDTO.setMedications(medList);
        return treatmentPlanDTO;
    }

    public static TreatmentPlan toEntity(TreatmentPlanDTO treatmentPlanDTO) {
        return new TreatmentPlan(treatmentPlanDTO.getFirsthour(),
                treatmentPlanDTO.getSecondhour(),
                treatmentPlanDTO.getPeriod());
    }
}
