package ro.andreea.ds2020.dtos.builders;
import org.springframework.beans.factory.annotation.Autowired;
import ro.andreea.ds2020.dtos.MedicationDTO;
import ro.andreea.ds2020.dtos.SideEffectsDTO;
import ro.andreea.ds2020.entities.Medication;
import ro.andreea.ds2020.entities.SideEffects;
import ro.andreea.ds2020.repositories.MedicationRepository;
import ro.andreea.ds2020.repositories.SideEffectsRepository;
import ro.andreea.ds2020.services.SideEffectsService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MedicationBuilder {

    @Autowired
    public static SideEffectsRepository sideEffectsRepository;

    private MedicationBuilder() {
    }

    public static MedicationDTO toMedicationDTO(Medication medication) {
        MedicationDTO medicationDTO = new MedicationDTO();
        List<String> side_effects = new ArrayList<String>();
        medicationDTO.setId(medication.getId());
        medicationDTO.setName(medication.getName());
        medicationDTO.setDosage(medication.getDosage());
        for (SideEffects sideEffects : medication.getSideEffects()) {
            side_effects.add(sideEffects.getName());
        }
        medicationDTO.setSideEffects(side_effects);
        return medicationDTO;
    }

    public static Medication toEntity(MedicationDTO medicationDTO) {
        Medication medication = new Medication();
        medication.setId(medicationDTO.getId());
        medication.setDosage(medicationDTO.getDosage());
        medication.setName(medicationDTO.getName());

        return medication;
    }
}
