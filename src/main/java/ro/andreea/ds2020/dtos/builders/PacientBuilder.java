package ro.andreea.ds2020.dtos.builders;
import ro.andreea.ds2020.dtos.PacientDTO;
import ro.andreea.ds2020.entities.Pacient;

public class PacientBuilder {

    private PacientBuilder() {
    }

    public static PacientDTO toPersonDTO(Pacient pacient) {
        return new PacientDTO(pacient.getId(), pacient.getName(), pacient.getBirthdate(), pacient.getGender(), pacient.getAddress(), pacient.getMedrecord(), pacient.getCusername(), pacient.getUsername());
    }

    public static Pacient toEntity(PacientDTO personDTO) {
        return new Pacient(personDTO.getId(),
                personDTO.getName(),
                personDTO.getBirthdate(),
                personDTO.getGender(),
                personDTO.getAddress(),
                personDTO.getMedrecord(),
                personDTO.getCusername(),
                personDTO.getUsername());
    }
}
