package ro.andreea.ds2020.dtos.builders;
import ro.andreea.ds2020.dtos.LoginDTO;
import ro.andreea.ds2020.dtos.SideEffectsDTO;
import ro.andreea.ds2020.entities.Login;
import ro.andreea.ds2020.entities.SideEffects;

public class LoginBuilder {

    private LoginBuilder() {
    }

    public static LoginDTO toLoginDTO(Login login) {
        return new LoginDTO(login.getId(),login.getUsername(), login.getPassword(), login.getRole());
    }

    public static Login toEntity(LoginDTO loginDTO) {
        return new Login(loginDTO.getUsername(),
                loginDTO.getPassword(),
                loginDTO.getRole());
    }
}
