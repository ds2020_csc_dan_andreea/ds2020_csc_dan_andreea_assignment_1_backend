package ro.andreea.ds2020.dtos.builders;
import ro.andreea.ds2020.dtos.CaregiverDTO;
import ro.andreea.ds2020.entities.Caregiver;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getId(), caregiver.getName(), caregiver.getBirthdate(), caregiver.getGender(), caregiver.getAddress(), caregiver.getUsername());
    }

    public static Caregiver toEntity(CaregiverDTO caregiverDTO) {
        return new Caregiver(caregiverDTO.getId(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthdate(),
                caregiverDTO.getGender(),
                caregiverDTO.getAddress(),
                caregiverDTO.getUsername());
    }
}