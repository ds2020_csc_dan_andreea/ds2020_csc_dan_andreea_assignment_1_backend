package ro.andreea.ds2020.dtos.builders;
import ro.andreea.ds2020.dtos.SideEffectsDTO;
import ro.andreea.ds2020.entities.SideEffects;

public class SideEffectsBuilder {

    private SideEffectsBuilder() {
    }

    public static SideEffectsDTO toSideEffectsDTO(SideEffects sideEffects) {
           return new SideEffectsDTO(sideEffects.getId(), sideEffects.getName());
    }

    public static SideEffects toEntity(SideEffectsDTO sideEffectsDTO) {
        return new SideEffects(sideEffectsDTO.getId(),
                sideEffectsDTO.getName());
    }
}
