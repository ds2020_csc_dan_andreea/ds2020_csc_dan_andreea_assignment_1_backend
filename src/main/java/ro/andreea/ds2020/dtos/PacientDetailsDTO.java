package ro.andreea.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.UUID;

public class PacientDetailsDTO {

    @NotNull
    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private Date birthdate;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    @NotNull
    public String medrecord;
    @NotNull
    private UUID idcaregiver;

    public PacientDetailsDTO(){

    }

    public PacientDetailsDTO(String name, Date birthdate, String gender, String address, String medrecord, UUID idcaregiver){
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;
        this.medrecord=medrecord;
        this.idcaregiver=idcaregiver;

    }

    public PacientDetailsDTO(UUID id, String name, Date birthdate, String gender, String address, String medrecord, UUID idcaregiver){
        this.id=id;
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;
        this.medrecord=medrecord;
        this.idcaregiver=idcaregiver;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedrecord() {
        return medrecord;
    }

    public void setMedrecord(String medrecord) {
        this.medrecord = medrecord;
    }

    public UUID getIdcaregiver() {
        return idcaregiver;
    }

    public void setIdcaregiver(UUID idCaregiver) {
        this.idcaregiver = idcaregiver;
    }
}

