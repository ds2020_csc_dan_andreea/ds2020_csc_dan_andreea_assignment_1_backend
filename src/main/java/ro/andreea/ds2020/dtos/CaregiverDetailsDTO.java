package ro.andreea.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.UUID;

public class CaregiverDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private Date birthdate;
    @NotNull
    private String gender;
    @NotNull
    private String address;

    public CaregiverDetailsDTO(){

    }

    public CaregiverDetailsDTO(String name, Date birthdate, String gender, String address){
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;
    }

    public CaregiverDetailsDTO(UUID id, String name, Date birthdate, String gender, String address){
        this.id=id;
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}


