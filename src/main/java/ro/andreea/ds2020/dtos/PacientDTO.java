package ro.andreea.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Objects;
import java.util.UUID;

public class PacientDTO {
    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private Date birthdate;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    @NotNull
    public String medrecord;
    @NotNull
    private String cusername;
    @NotNull
    private String username;
    private String role;

    public PacientDTO(){

    }

    public PacientDTO(UUID id, String name, Date birthdate, String gender, String address, String medrecord, String cusername, String username){
        this.id=id;
        this.name=name;
        this.birthdate=birthdate;
        this.gender=gender;
        this.address=address;
        this.medrecord=medrecord;
        this.cusername = cusername;
        this.username= username;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedrecord() {
        return medrecord;
    }

    public void setMedrecord(String medrecord) {
        this.medrecord = medrecord;
    }

    public String getCusername() {
        return cusername;
    }

    public void setCusername(String cusername) {
        this.cusername = cusername;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PacientDTO pacientDTO = (PacientDTO) o;
        return  Objects.equals(name, pacientDTO.name) &&
                Objects.equals(address, pacientDTO.address) &&
                Objects.equals(gender, pacientDTO.gender) &&
                Objects.equals(medrecord, pacientDTO.medrecord) &&
                Objects.equals(birthdate, pacientDTO.birthdate) &&
                Objects.equals(cusername, pacientDTO.cusername) &&
                Objects.equals(username, pacientDTO.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, gender, medrecord, birthdate, cusername, username );
    }
}

