package ro.andreea.ds2020.dtos;

import ro.andreea.ds2020.entities.Medication;
import ro.andreea.ds2020.entities.SideEffects;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class SideEffectsDTO {

    private UUID id;
    @NotNull
    private String name;

    public SideEffectsDTO(){

    }

    public SideEffectsDTO(String name){
        this.name=name;
    }

    public SideEffectsDTO(UUID id, String name){
        this.id=id;
        this.name=name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SideEffectsDTO sideEffectsDTO = (SideEffectsDTO) o;
        return  Objects.equals(name, sideEffectsDTO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}

