package ro.andreea.ds2020.dtos;

import ro.andreea.ds2020.entities.Medication;

import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class TreatmentPlanDTO {

    private UUID id;
    @NotNull
    private Set<String> medications;
    @NotNull
    private int firsthour;
    @NotNull
    private int secondhour;
    @NotNull
    private int period;
    @NotNull
    private String pacient_username;

    public String getPacient_username() {
        return pacient_username;
    }

    public void setPacient_username(String pacient_username) {
        this.pacient_username = pacient_username;
    }

    public TreatmentPlanDTO(){

    }

    public TreatmentPlanDTO(int firstHour, int secondHour, int period){

    }

    public TreatmentPlanDTO(Set<String> medications,int firsthour, int secondhour, int period){
        this.medications=medications;
        this.firsthour=firsthour;
        this.secondhour=secondhour;
        this.period=period;
    }

    public TreatmentPlanDTO(UUID id, Set<String> medications,int firsthour, int secondhour, int period, String pacient_username){
        this.id=id;
        this.medications=medications;
        this.firsthour=firsthour;
        this.secondhour=secondhour;
        this.period=period;
        this.pacient_username=pacient_username;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Set<String> getMedications() {
        return medications;
    }

    public void setMedications(Set<String> medications) {
        this.medications = medications;
    }

    public int getFirsthour() {
        return firsthour;
    }

    public void setFirsthour(int firsthour) {
        this.firsthour = firsthour;
    }

    public int getSecondhour() {
        return secondhour;
    }

    public void setSecondhour(int secondhour) {
        this.secondhour = secondhour;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreatmentPlanDTO treatmentPlanDTO =  (TreatmentPlanDTO) o;
        return  Objects.equals(medications, treatmentPlanDTO.medications) &&
                Objects.equals(firsthour, treatmentPlanDTO.firsthour) &&
                Objects.equals(secondhour, treatmentPlanDTO.secondhour) &&
                Objects.equals(period, treatmentPlanDTO.period);
    }

    @Override
    public int hashCode() {
        return Objects.hash(medications, firsthour, secondhour, period);
    }
}

