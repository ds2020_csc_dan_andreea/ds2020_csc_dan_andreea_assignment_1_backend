package ro.andreea.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.*;

public class MedicationDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private double dosage;
    @NotNull
    private List<String> side_effects;

    public MedicationDTO(){

    }

    public MedicationDTO(String name, double dosage, List<String> side_effects){
        this.name=name;
        this.dosage=dosage;
        this.side_effects= side_effects;
    }

    public MedicationDTO(UUID id, String name, double dosage, List<String> side_effects){
        this.id=id;
        this.name=name;
        this.dosage= dosage;
        this.side_effects= side_effects;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDosage() {
        return dosage;
    }

    public void setDosage(double dosage) {
        this.dosage = dosage;
    }

    public List<String> getSideEffects() {
        return side_effects;
    }

    public void setSideEffects(List<String> side_effects) {
        this.side_effects = side_effects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO medicationDTO = (MedicationDTO) o;
        return  Objects.equals(name, medicationDTO.name) &&
                Objects.equals(dosage, medicationDTO.dosage) &&
                Objects.equals(side_effects, medicationDTO.side_effects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, dosage, side_effects);
    }
}

