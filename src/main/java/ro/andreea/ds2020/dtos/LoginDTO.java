package ro.andreea.ds2020.dtos;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class LoginDTO {

    private UUID id;
    @NotNull
    private String username;
    @NotNull
    private String password;
    private String role;

    public LoginDTO(){

    }

    public LoginDTO(UUID id, String username, String password, String role){
        this.id=id;
        this.username= username;
        this.password =password;
        this.role=role;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginDTO loginDTO =(LoginDTO) o;
        return Objects.equals(username, loginDTO.username) &&
                Objects.equals(password, loginDTO.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }
}
