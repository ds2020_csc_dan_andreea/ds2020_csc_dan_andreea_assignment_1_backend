package ro.andreea.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.andreea.ds2020.dtos.TreatmentPlanDTO;
import ro.andreea.ds2020.services.TreatmentPlanService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/webapi/treatments")
public class TreatmentPlanController {

    public final TreatmentPlanService treatmentPlanService;

    @Autowired
    public TreatmentPlanController(TreatmentPlanService treatmentPlanService) {
        this.treatmentPlanService= treatmentPlanService;
    }

    @GetMapping()
    public ResponseEntity<List<TreatmentPlanDTO>> getTreatments() {
        List<TreatmentPlanDTO> dtos = treatmentPlanService.findAllTreatments();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<TreatmentPlanDTO> getTreatment(@PathVariable("id") UUID treatmentId) {
        TreatmentPlanDTO dto = treatmentPlanService.findTreatmentById(treatmentId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/patients/{username}")
    public ResponseEntity<List<TreatmentPlanDTO>>getTreatmentByPatientUsername(@PathVariable("username") String username) {
        List<TreatmentPlanDTO> dtos = treatmentPlanService.findTreatmentByPatientUsername(username);
        return new ResponseEntity<List<TreatmentPlanDTO>>(dtos, HttpStatus.OK);
    }

    @PostMapping(value= "/insert")
    public ResponseEntity<UUID> insertTreatmentPlan(@Valid @RequestBody TreatmentPlanDTO treatmentDTO) {
        UUID medicationID = treatmentPlanService.insertNewTreatment(treatmentDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @PutMapping(value= "/update")
    public ResponseEntity<UUID> updateTreatmentPlan(@Valid @RequestBody TreatmentPlanDTO treatmentDTO){
        UUID treatmentID = treatmentPlanService.updateTreatmentPlan(treatmentDTO);
        return new ResponseEntity<>(treatmentID, HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{id}")
    public ResponseEntity<UUID> deleteTreatment(@PathVariable UUID id) {
        UUID treatmentID =  treatmentPlanService.deleteTreatment(id);
        return new ResponseEntity<>(treatmentID, HttpStatus.OK);
    }
}
