package ro.andreea.ds2020.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.andreea.ds2020.dtos.MedicationDTO;
import ro.andreea.ds2020.services.MedicationService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping(value = "/webapi/medications")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedications() {
        List<MedicationDTO> dtos = medicationService.findAllMedications();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> getMedication(@PathVariable("id") UUID medicationId) {
        MedicationDTO dto = medicationService.findMedicationById(medicationId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/name/{name}")
    public ResponseEntity<MedicationDTO> getMedicationByName(@PathVariable("name") String medicationName) {
        MedicationDTO dto = medicationService.findMedicationByName(medicationName);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping(value= "/insert")
    public ResponseEntity<UUID> insertMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        System.out.println("IN INSERT METHOD");
        UUID medicationID = medicationService.insertNewMedication(medicationDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @PutMapping(value= "/update")
    public ResponseEntity<String> updateMedication(@Valid @RequestBody MedicationDTO medicationDTO){
        String medicationName = medicationService.updateExistingMedication(medicationDTO);
        return new ResponseEntity<>(medicationName, HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{name}")
    public ResponseEntity<String> deleteMedication(@PathVariable String name) {
        String medicationName = medicationService.deleteMedication(name);
        return new ResponseEntity<>(medicationName, HttpStatus.OK);
    }

}
