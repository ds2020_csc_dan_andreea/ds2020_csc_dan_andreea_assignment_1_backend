package ro.andreea.ds2020.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.andreea.ds2020.dtos.PacientDTO;
import ro.andreea.ds2020.services.PacientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping(value = "/webapi/patients")
public class PacientController {

    private final PacientService pacientService;

    @Autowired
    public PacientController(PacientService pacientService) {
        this.pacientService = pacientService;
    }

    @GetMapping()
    public ResponseEntity<List<PacientDTO>> getPersons() {
        List<PacientDTO> dtos = pacientService.findPacients();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PacientDTO> getPerson(@PathVariable("id") UUID pacientId) {
        PacientDTO dto = pacientService.findPacientById(pacientId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/username/{username}")
    public ResponseEntity<PacientDTO> getPerson(@PathVariable("username") String username) {
        PacientDTO dto = pacientService.findPacientByUsername(username);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value= "/caregiver/{username}")
    public ResponseEntity<List<PacientDTO>> getPacientsForCaregiver(@PathVariable("username") String caregiver_username){
        List<PacientDTO> dtos = pacientService.findPacientsByCaregiverUsername(caregiver_username);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(value= "/insert")
    public ResponseEntity<UUID> insertPacient(@Valid @RequestBody PacientDTO pacientDTO) {
        UUID personID = pacientService.insertNewPacient(pacientDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    @PutMapping(value= "/update")
    public ResponseEntity<UUID> updatePacient(@Valid @RequestBody PacientDTO pacientDTO){
        UUID personID = pacientService.updateExistingPacient(pacientDTO);
        return new ResponseEntity<>(personID, HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{username}")
    public ResponseEntity<String> deletePacient(@PathVariable String username){
        String personUsername = pacientService.deletePacient(username);
        return new ResponseEntity<>(personUsername, HttpStatus.OK);
    }

    //TODO: UPDATE, DELETE per resource

}
