package ro.andreea.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.andreea.ds2020.dtos.CaregiverDTO;
import ro.andreea.ds2020.dtos.LoginDTO;
import ro.andreea.ds2020.dtos.MedicationDTO;
import ro.andreea.ds2020.dtos.PacientDTO;
import ro.andreea.ds2020.dtos.builders.LoginBuilder;
import ro.andreea.ds2020.entities.Login;
import ro.andreea.ds2020.repositories.CaregiverRepository;
import ro.andreea.ds2020.repositories.PacientRepository;
import ro.andreea.ds2020.services.CaregiverService;
import ro.andreea.ds2020.services.LoginService;
import ro.andreea.ds2020.services.PacientService;

import javax.swing.text.html.parser.Entity;
import javax.validation.Valid;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/webapi/users")
public class LoginController {

    private final LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService){
        this.loginService =loginService;
    }
    @Autowired
    public PacientRepository pacientRepository;
    @Autowired
    public CaregiverRepository caregiverRepository;

    @PostMapping(value= "/login")
    public ResponseEntity<?> verifyLogin(@Valid @RequestBody LoginDTO loginDTO) {
        System.out.println("IN LOGIN METHOD");
        Login login = loginService.findLoginByUsername(loginDTO.getUsername());
        if(login!=null && login.getPassword().equals(loginDTO.getPassword())){
            if(login.getRole().equals("patient")){
                PacientService pacientService= new PacientService(pacientRepository);
                PacientDTO dto = pacientService.findPacientByUsername(login.getUsername());
                dto.setRole("patient");
                return new ResponseEntity<PacientDTO>(dto, HttpStatus.CREATED);
            }
            else if(login.getRole().equals(("caregiver"))){
                CaregiverService caregiverService = new CaregiverService(caregiverRepository);
                CaregiverDTO dto = caregiverService.findCaregiverByUsername(login.getUsername());
                dto.setRole("caregiver");
                return new ResponseEntity<CaregiverDTO>(dto, HttpStatus.CREATED);
            }
            else{
                LoginDTO dto= LoginBuilder.toLoginDTO(login);
                dto.setRole("doctor");
                return new ResponseEntity<LoginDTO>(dto, HttpStatus.CREATED);
            }
        }else{
            System.out.println("Wrong credentials!");
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
