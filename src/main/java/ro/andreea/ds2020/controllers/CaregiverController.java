package ro.andreea.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.andreea.ds2020.dtos.CaregiverDTO;
import ro.andreea.ds2020.services.CaregiverService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping(value = "/webapi/caregivers")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> getCaregivers() {
        System.out.println("INAINTE ");
        List<CaregiverDTO> dtos = caregiverService.findCaregivers();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiver(@PathVariable("id") UUID caregiverId) {
        CaregiverDTO dto = caregiverService.findCaregiverById(caregiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/username/{username}")
    public ResponseEntity<CaregiverDTO> getCaregiver(@PathVariable("username") String username) {
        CaregiverDTO dto = caregiverService.findCaregiverByUsername(username);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    @PostMapping("/insert")
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody CaregiverDTO caregiverDTO) {
        System.out.println("INAINTE");
        UUID caregiverID = caregiverService.insert(caregiverDTO);
        System.out.println("DUPA");
        return new ResponseEntity<UUID>(caregiverID, HttpStatus.CREATED);
    }

    @PutMapping(value= "/update")
    public ResponseEntity<UUID> updateCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO){
        UUID caregiverID = caregiverService.updateExistingCaregiver(caregiverDTO);
        return new ResponseEntity<>(caregiverID, HttpStatus.OK);
    }

    @DeleteMapping(value="/delete/{username}")
    public ResponseEntity<String> deleteCaregiver(@PathVariable String username){
        String personUsername = caregiverService.deleteCaregiver(username);
        return new ResponseEntity<>(personUsername, HttpStatus.OK);
    }
}

