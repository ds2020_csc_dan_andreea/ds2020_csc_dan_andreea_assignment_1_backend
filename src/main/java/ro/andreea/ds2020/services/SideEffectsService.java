package ro.andreea.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.andreea.ds2020.dtos.SideEffectsDTO;
import ro.andreea.ds2020.dtos.builders.LoginBuilder;
import ro.andreea.ds2020.dtos.builders.SideEffectsBuilder;
import ro.andreea.ds2020.entities.SideEffects;
import ro.andreea.ds2020.repositories.SideEffectsRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SideEffectsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SideEffectsService.class);
    public final SideEffectsRepository sideEffectsRepository;

    @Autowired
    public SideEffectsService(SideEffectsRepository sideEffectsRepository){
        this.sideEffectsRepository=sideEffectsRepository;
    }

    public List<SideEffectsDTO> findAllSideEffects(){
        List<SideEffects> sideEffectsList = sideEffectsRepository.findAll();
        return sideEffectsList.stream()
                .map(SideEffectsBuilder::toSideEffectsDTO)
                .collect(Collectors.toList());
    }

    public SideEffectsDTO findSideEffectByName(String sideEffectsName){
        SideEffects sideEffects = sideEffectsRepository.findByName(sideEffectsName);
        return SideEffectsBuilder.toSideEffectsDTO(sideEffects);
    }


}
