package ro.andreea.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.andreea.ds2020.dtos.MedicationDTO;
import ro.andreea.ds2020.dtos.TreatmentPlanDTO;
import ro.andreea.ds2020.dtos.builders.MedicationBuilder;
import ro.andreea.ds2020.dtos.builders.PacientBuilder;
import ro.andreea.ds2020.dtos.builders.TreatmentPlanBuilder;
import ro.andreea.ds2020.entities.Medication;
import ro.andreea.ds2020.entities.Pacient;
import ro.andreea.ds2020.entities.TreatmentPlan;
import ro.andreea.ds2020.repositories.MedicationRepository;
import ro.andreea.ds2020.repositories.PacientRepository;
import ro.andreea.ds2020.repositories.TreatmentPlanRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TreatmentPlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TreatmentPlanService.class);
    public final TreatmentPlanRepository treatmentPlanRepository;

    @Autowired
    public TreatmentPlanService(TreatmentPlanRepository treatmentPlanRepository) {
        this.treatmentPlanRepository= treatmentPlanRepository;
    }
    @Autowired
    public MedicationRepository medicationRepository;

    @Autowired
    public PacientRepository pacientRepository;

    public List<TreatmentPlanDTO> findAllTreatments(){
        List<TreatmentPlan> treatmentList = treatmentPlanRepository.findAll();
        return treatmentList.stream()
                .map(TreatmentPlanBuilder::toTreatmentPlanDTO)
                .collect(Collectors.toList());
    }

    public TreatmentPlanDTO findTreatmentById(UUID id) {
        Optional<TreatmentPlan> prosumerOptional = treatmentPlanRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Treatment plan with id {} was not found in db", id);
        }
        return TreatmentPlanBuilder.toTreatmentPlanDTO(prosumerOptional.get());
    }

    public List<TreatmentPlanDTO> findTreatmentByPatientUsername(String username){
        List<TreatmentPlan> treatmentPlanList = treatmentPlanRepository.findByPusername(username);
        return treatmentPlanList.stream()
                .map(TreatmentPlanBuilder::toTreatmentPlanDTO)
                .collect(Collectors.toList());
    }

    public UUID insertNewTreatment(TreatmentPlanDTO treatmentPlanDTO) {

        LOGGER.debug("Opening insertNewTreatment in TreatmentPlanService... ");
        ///create a new list with medications, that will hold the medications found on DB
        Set<Medication> medicationList = new HashSet<>();

        MedicationService medicationService= new MedicationService(medicationRepository);
        PacientService pacientService= new PacientService(pacientRepository);

        ///check if the username put in treatmentPlanDTO matches any data in the DB
        ///i need a method in the pacient service that returns a pacient with the given username
        Pacient pacient;
        try {
            pacient = PacientBuilder.toEntity(pacientService.findPacientByUsername(treatmentPlanDTO.getPacient_username()));
        }catch (NoSuchElementException noSuchElementException) {
            LOGGER.debug("No pacient found in the database!");
        }

        ///check if all the medications put in treatmentPlanDTO match any data in the DB
        for (String medication_name : treatmentPlanDTO.getMedications()) {
            for (MedicationDTO medicationDTO : medicationService.findAllMedications()) {
                if (medication_name.equals(medicationDTO.getName())) {
                    medicationList.add(MedicationBuilder.toEntity(medicationDTO));
                }
            }
        }

        if(medicationList.size()==0) return null;
        TreatmentPlan treatmentPlan = TreatmentPlanBuilder.toEntity(treatmentPlanDTO);
        treatmentPlan.setMedications(medicationList);
        treatmentPlan.setPusername(treatmentPlanDTO.getPacient_username());
        System.out.println("LALALALLA");
        this.treatmentPlanRepository.save(treatmentPlan);
        LOGGER.debug("New Treatment was succesfully inserted in db");
        LOGGER.debug("Closing insertNewTreatment in TreatmentPlanService... ");
        return treatmentPlan.getId();
    }

    public UUID updateTreatmentPlan(TreatmentPlanDTO treatmentPlanDTO){
        TreatmentPlan treatmentPlan= TreatmentPlanBuilder.toEntity(treatmentPlanDTO);
        if (treatmentPlanRepository.findById(treatmentPlan.getId())!=null) {
            treatmentPlanRepository.save(treatmentPlan);
        }
        return treatmentPlan.getId();
    }

    public UUID deleteTreatment(UUID id){
        treatmentPlanRepository.deleteById(id);
        return id;
    }
}
