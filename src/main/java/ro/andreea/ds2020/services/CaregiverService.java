package ro.andreea.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.andreea.ds2020.dtos.CaregiverDTO;
import ro.andreea.ds2020.dtos.builders.CaregiverBuilder;
import ro.andreea.ds2020.entities.Caregiver;
import ro.andreea.ds2020.repositories.CaregiverRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public List<CaregiverDTO> findCaregivers() {
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        return caregiverList.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDTO findCaregiverById(UUID id) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            //throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(prosumerOptional.get());
    }

    public CaregiverDTO findCaregiverByName(String name){
        Optional<Caregiver> prosumerOptional = caregiverRepository.findAllByName(name);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Caregiver with name {} was not found in db", name);
            //throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(prosumerOptional.get());
    }

    public CaregiverDTO findCaregiverByUsername(String username){
        Optional<Caregiver> prosumerOptional = caregiverRepository.findAllByUsername(username);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Caregiver with username {} was not found in db", username);
            //throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(prosumerOptional.get());
    }


    public UUID insert(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public UUID updateExistingCaregiver(CaregiverDTO caregiverDTO){
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        if (caregiverRepository.findByUsername(caregiver.getUsername()) != null) {
            Caregiver firstCaregiver = caregiverRepository.findByUsername(caregiver.getUsername());
            UUID wantedID = firstCaregiver.getId();
            caregiverRepository.delete(firstCaregiver);
            caregiverRepository.save(caregiver);
        }
        return caregiver.getId();
    }

    @Transactional
    public String deleteCaregiver(String username){
        caregiverRepository.deleteByUsername(username);
        return username;
    }
}
