package ro.andreea.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.andreea.ds2020.dtos.LoginDTO;
import ro.andreea.ds2020.dtos.builders.LoginBuilder;
import ro.andreea.ds2020.entities.Login;
import ro.andreea.ds2020.repositories.CaregiverRepository;
import ro.andreea.ds2020.repositories.LoginRepository;

import java.util.UUID;

@Service
public class LoginService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginService.class);
    private final LoginRepository loginRepository;

    @Autowired
    public LoginService(LoginRepository loginRepository){
        this.loginRepository = loginRepository;
    }

    public Login findLoginByUsername(String username){
        Login login = loginRepository.findByUsername(username);
        return login;
    }


}
