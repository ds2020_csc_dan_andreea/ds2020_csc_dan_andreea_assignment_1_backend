package ro.andreea.ds2020.services;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import ro.andreea.ds2020.dtos.MedicationDTO;
import ro.andreea.ds2020.dtos.SideEffectsDTO;
import ro.andreea.ds2020.dtos.builders.MedicationBuilder;
import ro.andreea.ds2020.dtos.builders.LoginBuilder;
import ro.andreea.ds2020.dtos.builders.SideEffectsBuilder;
import ro.andreea.ds2020.entities.Medication;
import ro.andreea.ds2020.entities.SideEffects;
import ro.andreea.ds2020.repositories.MedicationRepository;
import ro.andreea.ds2020.repositories.SideEffectsRepository;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PacientService.class);
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    @Autowired
    public SideEffectsRepository sideEffectsRepository;

    public List<MedicationDTO> findAllMedications(){
        List<Medication> medicationList = medicationRepository.findAll();
        return medicationList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public MedicationDTO findMedicationById(UUID id) {
        Optional<Medication> prosumerOptional = medicationRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            //throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return MedicationBuilder.toMedicationDTO(prosumerOptional.get());
    }

    public MedicationDTO findMedicationByName(String name) {
        Optional<Medication> prosumerOptional = medicationRepository.findAllByName(name);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with name {} was not found in db", name);
            //throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return MedicationBuilder.toMedicationDTO(prosumerOptional.get());
    }

    public UUID insertNewMedication(MedicationDTO medicationDTO) {
        List<SideEffects> sideEffects = new ArrayList<SideEffects>();
        SideEffectsService sideEffectsService = new SideEffectsService(sideEffectsRepository);

        for(String name: medicationDTO.getSideEffects()){
            for(SideEffectsDTO sideEffectsDTO: sideEffectsService.findAllSideEffects()){
                if(name.equals(sideEffectsDTO.getName())){
                    System.out.println("SIDE EFFECT: \n"+sideEffectsDTO.getName());
                    sideEffects.add(SideEffectsBuilder.toEntity(sideEffectsDTO));
                }
            }
        }

        if(sideEffects.size()==0) return null;
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        medication.setSideEffects(sideEffects);
        this.medicationRepository.save(medication);
        LOGGER.debug("New medication was succesfully inserted in db");
        LOGGER.debug("Closing insertNewMedication in MedicationService... ");
        return medication.getId();
    }

    public String updateExistingMedication(MedicationDTO medicationDTO){
        System.out.println("MNA"+medicationDTO.getSideEffects());
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        if (medicationRepository.findByName(medication.getName())!=null) {
            Medication medicationFirst = medicationRepository.findByName(medication.getName());
            medicationRepository.delete(medicationFirst);

            List<SideEffects> sideEffectsList = new ArrayList<>();
            for(String name: medicationDTO.getSideEffects()){
                for(SideEffects sideEffects: sideEffectsRepository.findAll()){
                    if(name.equals(sideEffects.getName())){
                        sideEffectsList.add(sideEffects);
                    }
                }
            }
            medication.setSideEffects(sideEffectsList);
            medicationRepository.save(medication);
        }
        return medication.getName();
    }

    @Transactional
    public String deleteMedication(String name){
        MedicationDTO medicationDTO = findMedicationByName(name);
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        medication.setSideEffects(null);
        medicationRepository.save(medication);
        medicationRepository.deleteByName(name);
        return name;
    }
}
