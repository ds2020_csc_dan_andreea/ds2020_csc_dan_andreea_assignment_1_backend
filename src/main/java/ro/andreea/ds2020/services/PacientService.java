package ro.andreea.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.andreea.ds2020.dtos.CaregiverDTO;
import ro.andreea.ds2020.dtos.PacientDTO;
import ro.andreea.ds2020.dtos.builders.PacientBuilder;
import ro.andreea.ds2020.entities.Pacient;
import ro.andreea.ds2020.repositories.CaregiverRepository;
import ro.andreea.ds2020.repositories.PacientRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PacientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PacientService.class);
    private final PacientRepository pacientRepository;

    @Autowired
    public PacientService(PacientRepository pacientRepository) {
        this.pacientRepository = pacientRepository;
    }

    @Autowired
    public CaregiverRepository caregiverRepository;

    public List<PacientDTO> findPacients() {
        List<Pacient> personList = pacientRepository.findAll();
        return personList.stream()
                .map(PacientBuilder::toPersonDTO)
                .collect(Collectors.toList());
    }

    public PacientDTO findPacientById(UUID id) {
        Optional<Pacient> prosumerOptional = pacientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            //throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PacientBuilder.toPersonDTO(prosumerOptional.get());
    }

    public List<PacientDTO> findPacientsByCaregiverUsername(String caregiver_username){
        List<Pacient> pacientList = pacientRepository.findAllByCusername(caregiver_username);
        return pacientList.stream()
                .map(PacientBuilder::toPersonDTO)
                .collect(Collectors.toList());
    }

    public PacientDTO findPacientByName(String username){
        Optional<Pacient> prosumerOptional = pacientRepository.findAllByName(username);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with name {} was not found in db", username);
            //throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PacientBuilder.toPersonDTO(prosumerOptional.get());
    }

    public PacientDTO findPacientByUsername(String username){
        Optional<Pacient> prosumerOptional = pacientRepository.findByUsername(username);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with name {} was not found in db", username);
            //throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PacientBuilder.toPersonDTO(prosumerOptional.get());
    }


    public UUID insertNewPacient(PacientDTO pacientDTO) {
        Pacient pacient = PacientBuilder.toEntity(pacientDTO);
        pacient = pacientRepository.save(pacient);
        LOGGER.debug("Person with id {} was inserted in db", pacient.getId());
        return pacient.getId();
    }

    public UUID updateExistingPacient(PacientDTO pacientDTO){
        Pacient pacient = PacientBuilder.toEntity(pacientDTO);
        if (pacientRepository.findByUsername(pacient.getUsername())!=null) {
            Optional<Pacient> firstPatient = pacientRepository.findByUsername(pacient.getUsername());
            Pacient ffirstPatient = firstPatient.get();
            pacientRepository.delete(ffirstPatient);
            pacientRepository.save(pacient);
        }
        return pacient.getId();
    }

    @Transactional
    public String deletePacient(String username){
        pacientRepository.deleteByUsername(username);
        return username;
    }
}
